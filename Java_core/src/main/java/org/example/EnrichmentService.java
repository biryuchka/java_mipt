package org.example;
import com.google.gson.Gson;
import com.google.gson.*;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class EnrichmentService {
    private final ConcurrentMap<String, ConcurrentMap<String, String>> Enrichments;
    EnrichmentService(ConcurrentMap<String, ConcurrentMap<String, String>> Enrichments) {
        this.Enrichments = Enrichments;
    }

    // возвращается обогащенный (или необогащенный content сообщения
    public String enrich(Message message){
        JsonObject jsonObject = JsonParser.parseString(message.content).getAsJsonObject();
        if (message.enrichmentType == Message.EnrichmentType.MSISDN) {
            message.content = find(jsonObject).toString();
        }
        return message.content;
    }
    public JsonObject find(JsonObject jsonObject) {
        String msisdn = String.valueOf(jsonObject.get("msisdn")).replace("\"", "");
        Map<String, String> info = this.Enrichments.get(msisdn);
        if (info != null) {
            JsonElement jsonMap = JsonParser.parseString((new Gson()).toJson(info));
            jsonObject.add("enrichment", jsonMap);
        }
        return jsonObject;
    }
}