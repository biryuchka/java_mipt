package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class App {
   public static void main(String[] args) {
       ConcurrentMap<String, ConcurrentMap <String, String>> map = new ConcurrentHashMap<>();
       ConcurrentMap<String, String> tmp = new ConcurrentHashMap<>();
       tmp.put("firstName", "Vasya");
       tmp.put("secondName", "Petrov");
       map.put("81000000001", tmp);
       String JSON = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000001\"}";
       Message message = new Message();
       message.content = JSON;
       message.enrichmentType = Message.EnrichmentType.MSISDN;
       EnrichmentService service = new EnrichmentService(map);
       System.out.println(service.enrich(message));
   }
}
