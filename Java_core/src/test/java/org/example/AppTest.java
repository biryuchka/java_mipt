package org.example;

import org.testng.annotations.Test;

import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit test for simple App.
 */
public class AppTest
{
    public void testApp() throws ExecutionException, InterruptedException
    {
        ConcurrentMap<String, ConcurrentMap <String, String>> people = new ConcurrentHashMap<>();
        ConcurrentMap<String, String> tmp = new ConcurrentHashMap<>();
        tmp.put("firstName", "Vasya");
        tmp.put("secondName", "Petrov");
        people.put("81000000001", tmp);
        tmp = new ConcurrentHashMap<>();
        tmp.put("firstName", "Nikolay");
        tmp.put("secondName", "Ivanov");
        people.put("81000000002", tmp);
        tmp = new ConcurrentHashMap<>();
        tmp.put("firstName", "Mihail");
        tmp.put("secondName", "Zubkov");
        people.put("81000000003", tmp);
        EnrichmentService enrichmentService = new EnrichmentService(people);
        String JSON1 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000001\"}";
        String JSON2 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000000\"}";
        String JSON3 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000003\"}";
        Message message1 = new Message();
        message1.content = JSON1;
        message1.enrichmentType = Message.EnrichmentType.MSISDN;
        Message message2 = new Message();
        message2.content = JSON2;
        message2.enrichmentType = Message.EnrichmentType.MSISDN;
        Message message3 = new Message();
        message3.content = JSON3;
        message3.enrichmentType = Message.EnrichmentType.MSISDN;
        ExecutorService executorThreads = Executors.newFixedThreadPool(5);
        Future<String> future1 = executorThreads.submit(() -> enrichmentService.enrich(message1));
        Future<String> future2 = executorThreads.submit(() -> enrichmentService.enrich(message2));
        Future<String> future3 = executorThreads.submit(() -> enrichmentService.enrich(message3));
        String response1 = future1.get();
        String response2 = future2.get();
        String response3 = future3.get();
        String exampleResponse1 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000001\",\"enrichment\":{\"firstname\":\"Vasya\",\"lastname\":\"Petrov\"}}";
        String exampleResponse2 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000000\"}";
        String exampleResponse3 = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000003\",\"enrichment\":{\"firstname\":\"Mihail\",\"lastname\":\"Zubkov\"}}";
        assertEquals(exampleResponse1, response1);
        assertEquals(exampleResponse2, response2);
        assertEquals(exampleResponse3, response3);
    }
}
