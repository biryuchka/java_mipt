package org.example;
class Tiger implements LandAnimals, Predators, Animals {
    String name, eat, doing;

    Tiger() {
        this.name = "Тигр";
        SetWalk();
        SetEatMeat();
    }
    @Override
    public void SetWalk() {
        this.doing = "ходит";
    }

    @Override
    public void SetEatMeat() {
        this.eat = "ест мясо";
    }

    @Override
    public void CanEat() {
        System.out.println(this.name + " " + this.eat + "\n");
    }

    @Override
    public void CanDo() {
        System.out.println(this.name + " " + this.doing + "\n");
    }
}
