package org.example;
class Horse implements LandAnimals, Herbivores, Animals {
    String name, eat, doing;

    Horse() {
        this.name = "Лошадь";
        SetWalk();
        SetEatGreen();
    }
    @Override
    public void SetWalk() {
        this.doing = "ходит";
    }

    @Override
    public void SetEatGreen() { this.eat = "ест траву"; }

    @Override
    public void CanEat() {
        System.out.println(this.name + " " + this.eat + "\n");
    }

    @Override
    public void CanDo() {
        System.out.println(this.name + " " + this.doing + "\n");
    }

}
