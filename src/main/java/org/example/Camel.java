package org.example;
class Camel implements LandAnimals, Herbivores, Animals {
    String name, eat, doing;

    Camel() {
        this.name = "Верблюд";
        SetWalk();
        SetEatGreen();
    }
    @Override
    public void SetWalk() {
        this.doing = "ходит";
    }

    @Override
    public void SetEatGreen() { this.eat = "ест траву"; }

    @Override
    public void CanEat() {
        System.out.println(this.name + " " + this.eat + "\n");
    }

    @Override
    public void CanDo() {
        System.out.println(this.name + " " + this.doing + "\n");
    }

}
