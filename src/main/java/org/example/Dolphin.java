package org.example;
class Dolphin implements SwimmingAnimals, Predators, Animals {
    String name, eat, doing;

    Dolphin() {
        this.name = "Дельфин";
        SetSwim();
        SetEatMeat();
    }
    @Override
    public void SetSwim() {
        this.doing = "плавает";
    }

    @Override
    public void SetEatMeat() {
        this.eat = "ест рыбу";
    }

    @Override
    public void CanEat() {
        System.out.println(this.name + " " + this.eat + "\n");
    }

    @Override
    public void CanDo() {
        System.out.println(this.name + " " + this.doing + "\n");
    }
}
