package org.example;
class Eagle implements FlyingAnimals, Predators, Animals {
    String name, eat, doing;

    Eagle() {
        this.name = "Орел";
        SetFly();
        SetEatMeat();
    }

    @Override
    public void SetEatMeat() { this.eat = "ест мясо"; }

    @Override
    public void SetFly() { this.doing = "летает"; }

    @Override
    public void CanEat() {
        System.out.println(this.name + " " + this.eat + "\n");
    }

    @Override
    public void CanDo() {
        System.out.println(this.name + " " + this.doing + "\n");
    }

}
