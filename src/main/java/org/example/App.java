package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Tiger tiger = new Tiger();
        Dolphin dolphin = new Dolphin();
        Camel camel = new Camel();
        Horse horse = new Horse();
        Eagle eagle = new Eagle();

        tiger.CanDo();
        tiger.CanEat();

        dolphin.CanDo();
        dolphin.CanEat();

        camel.CanDo();
        camel.CanEat();

        horse.CanDo();
        horse.CanEat();

        eagle.CanDo();
        eagle.CanEat();
    }
}
